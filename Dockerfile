FROM caddy:2-builder AS builder

RUN xcaddy build \
  --with github.com/mholt/caddy-ratelimit \
  --with github.com/caddy-dns/hetzner

FROM caddy:2-alpine

COPY --from=builder /usr/bin/caddy /usr/bin/caddy
